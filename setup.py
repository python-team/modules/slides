"""Distutils installer for Slides"""

from distutils.core import setup
import slides

setup(name="slides",
      version=slides.__version__,
      description="HTML presentation generator",
      author="Moshe Zadka, Itamar Shtull-Trauring",
      author_email="python@itamarst.org",
      url="http://itamarst.org/software/slides/",
      py_modules=["slides"])
